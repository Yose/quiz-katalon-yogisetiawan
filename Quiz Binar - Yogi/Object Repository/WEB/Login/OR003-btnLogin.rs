<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR003-btnLogin</name>
   <tag></tag>
   <elementGuidId>68c894c2-9509-46d2-9393-719e34958bbe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'login']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#login</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login</value>
      <webElementGuid>2b7eb11e-530c-4698-ad5b-520fe07909f1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
